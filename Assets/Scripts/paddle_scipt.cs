﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class paddle_scipt : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "BarrierUp")
        {
            GameManager.isScoring = true;
            GameManager.scorePlayer2++;
            ResetPos();
        }

        if (collision.gameObject.tag == "BarrierDown")
        {
            GameManager.isScoring = true;
            GameManager.scorePlayer1++;
            ResetPos();
        }
    }

    void ResetPos()
    {
        transform.position = Vector2.zero;
    }

}
