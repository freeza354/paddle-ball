﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static int scorePlayer1, scorePlayer2;
    public static bool isScoring;

    public int scoreMax;
    public Text player1notif, player2notif, player1Score, player2Score;
    public GameObject panel;

    // Start is called before the first frame update
    void Start()
    {
        scorePlayer1 = 0;
        scorePlayer2 = 0;

        player1Score.text = "0";
        player2Score.text = "0";

        isScoring = false;
        
        panel.SetActive(false);
        player1notif.gameObject.SetActive(false);
        player2notif.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (isScoring)
        {
            isScoring = false;
            player1Score.text = scorePlayer1.ToString();
            player2Score.text = scorePlayer1.ToString();
        }

        if (scorePlayer1 == scoreMax || scorePlayer2 == scoreMax)
        {
            panel.SetActive(true);
            player1notif.gameObject.SetActive(true);
            player2notif.gameObject.SetActive(true);

            if (scorePlayer1 == scoreMax)
            {
                player1notif.text = "Win";
                player2notif.text = "Lose";
            }
            else
            {
                player1notif.text = "Lose";
                player2notif.text = "Win";
            }

            StartCoroutine(backToMenu());
        }

    }

    IEnumerator backToMenu()
    {
        yield return new WaitForSeconds(1.2f);
        SceneManager.LoadScene("menu");
    }

}
