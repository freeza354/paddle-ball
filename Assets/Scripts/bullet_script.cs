﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bullet_script : MonoBehaviour
{
    public float moveSpeed, timeToDestroy;
    public bool isMirror;

    private Rigidbody2D rb;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();

        if (isMirror)
        {
            rb.AddForce(Vector2.down * moveSpeed);
        }
        else
        {
            rb.AddForce(Vector2.up * moveSpeed);
        }

        StartCoroutine(DestroyBall(timeToDestroy));
    }

    private void Update()
    {
        if (GameManager.isScoring)
        {
            Destroy(gameObject);
        }
    }

    IEnumerator DestroyBall(float timer)
    {
        yield return new WaitForSeconds(timer);
        Destroy(gameObject);
    }

}
