﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class menu_manager : MonoBehaviour
{

    public GameObject panelQuit;

    private void Start()
    {
        panelQuit.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            panelQuit.gameObject.SetActive(true);
        }

        if (Input.anyKey && !panelQuit.gameObject.activeInHierarchy)
        {
            SceneManager.LoadScene("main");
        }   
    }

    public void yesButton()
    {
        Application.Quit();
    }

    public void noButton()
    {
        panelQuit.gameObject.SetActive(false);
    }

}
