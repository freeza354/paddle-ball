﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class player_script : MonoBehaviour
{
    public float moveSpeed, timeReload;
    public GameObject bulletGO;

    private bool isWalking;
    private Rigidbody2D rb;
    private Vector2 direction;
    private float timeReloadTemp;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        timeReloadTemp = timeReload;
    }

    // Update is called once per frame
    void Update()
    {
        direction = new Vector2(Input.GetAxis("Horizontal"), 0);
        reload();

        if (Input.GetKeyDown(KeyCode.W) && timeReload == 0)
        {
            shoot();
            timeReload = timeReloadTemp;
        }

        timeReload -= Time.time;
    }

    private void FixedUpdate()
    {
        moveCharacter(direction);
    }

    void moveCharacter(Vector2 dir) {
        rb.velocity = dir * moveSpeed;
    }

    void shoot()
    {
        GameObject go = Instantiate(bulletGO, transform.position, transform.rotation);
    }

    void reload()
    {
        if (timeReload <= 0)
        {
            timeReload = 0;
        }
    }

}
